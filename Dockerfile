FROM python:3.6-alpine

ADD HoeWarmisHetinDelft.py /
COPY requirements.txt /

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["python", "HoeWarmisHetinDelft.py"]