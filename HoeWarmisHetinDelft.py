import time
import requests


def get_data():
    ts = int(time.time() * 1000)
    url = f'https://weerindelft.nl/clientraw.txt?{ts}'
    try:
        page = requests.get(url)
        if page.status_code == 200:
            return f'{page.text.split(" ")[4]} degrees Celsius'
        else:
            return 'Server not responded'
    except requests.exceptions.ConnectionError:
        return 'Connection error'


if __name__ == '__main__':
    print(get_data())
